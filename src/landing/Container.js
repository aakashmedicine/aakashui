import Header from "./header/Header";
import Body from "./body/Body";
import Footer from "./footer/Footer";

function Container() {
  return (
    <>
      <Header /> <Body /> <Footer />
    </>
  );
}

export default Container;
