import { Typography } from '@mui/material';
import './Header.scss';
import TextField from '@mui/material/TextField';

function Header() {
  return (
    <div className="Header">
      <div className='center-container'>
        <Typography variant="h5">Lorem Ipsum Private Limited</Typography>
      <TextField
          label="Search for medicines and wellness products"
          type="search"
          variant="outlined"
          size='small'
          className='search-field'
        />
      </div>
    </div>
  );
}

export default Header;
