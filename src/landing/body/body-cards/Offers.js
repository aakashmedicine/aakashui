import { Typography } from "@mui/material";
import "./ItemCard.scss";

function Offers() {
  return (
    <div className="card-body">
      <Typography variant="h6">Offers / Promotions</Typography>
      <Typography className="card-item">20% off on 1st order</Typography>
      <Typography className="card-item">Free delivery</Typography>
    </div>
  );
}

export default Offers;
