import { Typography } from "@mui/material";
import "./ItemCard.scss";

function NewArrivals() {
  return (
    <div className="card-body">
      <Typography variant="h6">Advertisement New Arrivals</Typography>
      <Typography className="card-item">Super health kit</Typography>
    </div>
  );
}

export default NewArrivals;
