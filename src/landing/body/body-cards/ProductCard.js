import { Typography } from "@mui/material";
import "./ItemCard.scss";

function ProductCard() {
  return (
    <div className="card-body">
      <Typography className="card-item">Medicine</Typography>
      <Typography className="card-item">Wellness</Typography>
      <Typography className="card-item">Beauty care</Typography>
    </div>
  );
}

export default ProductCard;
