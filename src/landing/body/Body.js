import { Typography } from "@mui/material";
import "./Body.scss";
import Paper from "@mui/material/Paper";
import ProductCard from "./body-cards/ProductCard";
import Offers from "./body-cards/Offers";
import NewArrivals from "./body-cards/NewArrivals";

function Body() {
  return (
    <div className="main-body">
      <div className="top-section">
        <Typography>Medicine</Typography>
        <Typography>Wellness</Typography>
        <Typography>Beauty</Typography>
      </div>
      <div className="main-section">
        <div className="card-container">
          <Paper elevation={3} className="paper-name">
            <ProductCard />
          </Paper>
          <Paper elevation={3} className="paper-name">
            <Offers />
          </Paper>
          <Paper elevation={3} className="paper-name">
            <NewArrivals />
          </Paper>
        </div>
      </div>
    </div>
  );
}

export default Body;
