import { Typography } from "@mui/material";
import "./Footer.scss";

function Footer() {
  const aboutAakash =
    "Lorem Ipsum is one of India's most trusted pharmacies, dispencing quality medicines at reasonable prices to over 1 million happy customers - PAN India";
  return (
    <div className="Footer">
      <Typography variant="h6" className="footer-text">{aboutAakash}</Typography>
      <div className="contacts">
        <div>100% Quality assured</div>
        <div>Gurgam-India</div>
        <div>customersupport@lorem.in</div>
      </div>
    </div>
  );
}

export default Footer;
